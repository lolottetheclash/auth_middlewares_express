const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const jwt = require('jsonwebtoken');
let config = require('./knexfile.js')
let database = knex(config.development)
const bcrypt = require('bcrypt')


// Create a feathers instance.
const app = express(feathers())

app.use(express.text())
// Turn on JSON parser for REST services
app.use(express.json())
// Turn on URL-encoded parser for REST services
// recupère les infos quand on soumet un formulaire, récupères des datas de typer url-encoded
app.use(express.urlencoded({ extended: true }))
// Enable REST services
app.configure(express.rest());





// CREATE AN ACCOUNT
app.post('/create-account', function (request, response) {
  // Check if the request is for the middelware  
    // POST data is in request.body
    let data = request.body
    async function saveUser() {
      try {
        let user = await database.into('user').insert({email : data.email, fullname : data.name})
        const payload = { "email" : data.email, "fullname" : data.name  }
        const secret = "my super secret";
        const token = jwt.sign(payload, secret, { algorithm: "HS256" }, function(err, token) {
          if (err){
            console.log(err)
          } else {
          response.redirect('saved.html')
          console.log(`Merci de confirmer votre inscription en cliquant sur ce lien : http://localhost:3000/confirm-registration?token=${token}`);
        }});
      } catch(e) {
        response.redirect('./errors/error_registration.html')
      }
    }    
    saveUser()
})


// CONFIRM REGISTRATION
app.get('/confirm-registration', function(request, response){
    // on récupère les paramètres passés dans l'url (ici, le token)
    let data = request.query
    // On vérifie que le token contient bien les informations du user
    function autorization (token){
      try {
        jwt.verify(data.token, 'my super secret', function(err, decoded) {
          if (err){
            console.log(err)
          } else {
          let {email, fullname} = decoded
          response.send(`<h1>${fullname}, merci de définir votre mot de passe </h1>
          <form action="/activate-account" method="POST" class="form-example">
              <div class="form-example">
                  <label for="password">Password (8 characters minimum):</label>
                  <input type="password" id="password" name="password" minlength="8" required>
              </div>
              <div class="form-example">
                  <label for="password">Password (8 characters minimum):</label>
                  <input type="password" id="password" name="password" minlength="8" required>
              </div>
              <input id="email" name="email" type="hidden" value=${email}>
              <div class="form-example">
              <input type="submit" value="Submit">
              </div>
          </form>`)
        }});
      } catch(e) {
        response.send("Problème d'authentification")
      }
    }
    autorization(data.token)
})


// CREATION DU PASSWORD
app.post('/activate-account', function(request, response) {
  let dataMail = request.body.email
  async function userPass(){
    if (request.body.password[0] === request.body.password[1]){
      let saltRounds = 10
      let hash = bcrypt.hashSync(request.body.password[0], saltRounds)  
      await database.into('user').update({active: true, password: hash}).where({email : dataMail })
      response.redirect('login.html')
    } else {
      response.redirect('errors/error_password.html')
    }
  }
  userPass()
})

// LOGIN
app.post('/login', function (request, response) {
  let data = request.body
  async function passUser(){
    try{
      let user = await database.select('password').from('user').where({email : data.email})
      bcrypt.compareSync(data.password, user[0].password)
      const payload = data.email
      const secret = 'mon super secret de ouf'
      const token = jwt.sign(payload, secret, { algorithm: "HS256" })
      response.cookie(token)
      console.log(response)
      console.log(response.cookie)
      response.redirect('home.html') 
    } catch(e) {
      response.redirect('errors/error_login.html')
    }
  }
  passUser()
})

/*
creer un middelware de securité qui vérifie que le token est bien signé (par cookie) response.cookie(nom du cookie : token)
est ce un admin? ok next sinon on fait un send status (403) accès refusé
faire les requêtes avec Postman pour pouvoir tester les cookies
Activités tutorées :
ajouter un middleware implémentant des règles d’accès aux ressources users et pictures
tous les accès /api/users et /api/pictures nécessitent un token JWT signé et contenant le user_id
un utilisateur authentifié avec isAdmin=true a accès à toutes les ressources sans restriction
un utilisateur avec is_admin=false n’a accès qu’à son propre enregistrement dans /api/users
un utilisateur avec is_admin=false n’a accès qu’à ses propres photos dans /api/pictures*/


app.use('/api/pictures', service({
  Model: database,
  name: 'Picture'
}))

app.use('/api/users', service({
  Model: database,
  name: 'User'
}))

// On dit à Express où se trouvent les pages statiques
// On accède aux pages statiques sur http://localhost:3000/index.html (public/index.html)
app.use(express.static('./public'));

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})





